
import * as React from 'react';
import { View, Text, StyleSheet, Alert, Image} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as Sentry from "@sentry/react-native";
import { TextInput, Button } from 'react-native-paper';
import { height, width } from '@mui/system';

// to be used to test.
const transaction = Sentry.startTransaction({
  op: "test",
  name: "My First Test Transaction",
});


const Form = () => {
  const [firstName, setfirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [email, setEmail] = React.useState('');
  
  return (
    <View>
    <TextInput
      label="First Name"
      value={firstName}
      onChangeText={text => setfirstName(text)}
    />
    <TextInput
      label="Last Name"
      value={lastName}
      onChangeText={text => setLastName(text)}
    />
    <TextInput
      label="Last Name"
      value={email}
      onChangeText={text => setEmail(text)}
    />
    <Button style={styles.button}
    color={styles.button.color}
    icon="send"
    onPress={() => Alert.alert('Submission Succuss')}
  >Submit</Button>
    </View>
  );
};

function FormScreen({navigation}) {
  return (
    <View>
      <Text style={styles.text}>Home Screen</Text>
      <Button style={styles.button}
      icon="home"
      color={styles.button.color}
      onPress={() => navigation.navigate('Home')}
    >Home page</Button>
    <Form />
    </View>
  );
}

function CostumeScreen({navigation}) {
  return (
    <View style={styles.body}>
      <Text style={styles.text}>Costume Screen</Text>
      <Image
      source={{ uri: 'https://s3.eu-west-1.amazonaws.com/www.mahaseel.net/images/Mahaseel-web-logo-en.png' }}
      style={{ width: 100, height: 100 }}
    />
      <Button style={styles.button}
      icon="home"
      color={styles.button.color}
      onPress={() => navigation.navigate('Home')}
    >Go to Home</Button>
    </View>
  );
}

// src="https://s3.eu-west-1.amazonaws.com/www.mahaseel.net/images/Mahaseel-web-logo-en.png"

function HomeScreen({navigation}) {
  const [name, setName] = React.useState('Anonyms')
  return (
    <View style={styles.body}>
      <Text style={styles.text}>Home page</Text>
      <TextInput
      style={{height:50, width:200, marginBottom:25}}
      label="Please inter Your name:"
      value={name}
      onChangeText={text => setName(text)}
    />
    <Text style={{marginBottom:15, fontSize:25, color:"blue"}} >Welcome {name}!</Text>
      <Button style={styles.button}
      color={styles.button.color}
      onPress={() => navigation.navigate('Form')}>Go to Form
    </Button>
    <Button style={styles.button}
    color={styles.button.color}
      title="Go to Form"
      onPress={() => navigation.navigate('Costume')}
      >Costume Screen
      </Button>
    </View>
  );
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeScreen">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Costume" component={CostumeScreen} />
        <Stack.Screen name="Form" component={FormScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
body:{
  flex: 1, alignItems: 'center', justifyContent: 'center' 
},
text:{
  color:'red',
  fontSize: 36,
  margin:20
},
button:{
  color: '#ffffff',
  fontSize: 26,
  margin: 5,
  padding:10,
  backgroundColor: 'red',
},
})

export default App;